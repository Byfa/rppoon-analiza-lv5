﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            User user1, user2;
            user1 = User.GenerateUser("User one");
            user2 = User.GenerateUser("User two");

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();

            Dataset dataset = new Dataset("datoteka.csv");
            Console.WriteLine("Output using object from class Dataset:");
            dataConsolePrinter.ConsolePrinter(dataset);

            Console.WriteLine("\nOutput using object from class Dataset using virtual Proxy:");
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("datoteka.csv");
            dataConsolePrinter.ConsolePrinter(virtualProxyDataset);

            Console.WriteLine("\nOutput using object from class Datase tusing protection Proxy, user ID=1:");
            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user1);
            dataConsolePrinter.ConsolePrinter(protectionProxyDataset);

            Console.WriteLine("\nOutput using object from class Datase tusing protection Proxy, user ID=2:");
            protectionProxyDataset = new ProtectionProxyDataset(user2);
            dataConsolePrinter.ConsolePrinter(protectionProxyDataset);
            //posto je dva, ne moze se ispisati jer GetData() daje null

            Console.ReadKey();
        }
    }
}
