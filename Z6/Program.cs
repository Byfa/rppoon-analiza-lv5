﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            RedTheme redTheme = new RedTheme();
            LightTheme lightTheme = new LightTheme();
            GroupNote badGroupNote = new GroupNote("Welcome to HELL!","Satan", redTheme);
            badGroupNote.Add("Luci");
            badGroupNote.Add("Sam");
            badGroupNote.Show();
            GroupNote goodGroupNote = new GroupNote("AW shit, here we go again", "Dean", lightTheme);
            goodGroupNote.Add("Castiel");
            goodGroupNote.Show();

            badGroupNote.Remove("Sam");
            badGroupNote.Show();
            goodGroupNote.Add("Sam");
            goodGroupNote.Show();


            Console.ReadKey();
        }
    }
}
