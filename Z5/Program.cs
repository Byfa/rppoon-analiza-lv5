﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            RedTheme redTheme = new RedTheme();
            ReminderNote note = new ReminderNote("WOooWWWWW I won't let you gooOOOo", lightTheme);
            note.Show();
            note = new ReminderNote("Welcome to HELL", redTheme);
            note.Show();
            Console.ReadKey();
        }
    }
}
