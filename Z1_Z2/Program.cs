﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            Product book = new Product("Overlord",0.5, 17.99);
            Product lamp = new Product("Silver lamp", 1.5, 9.99);

            Box listProducts = new Box("products");
            listProducts.Add(book);
            listProducts.Add(lamp);

            Console.WriteLine("Product price:"+listProducts.Price);
            Console.WriteLine("Product weight:"+listProducts.Weight);
            Console.WriteLine("Product description:"+listProducts.Description());

            ShippingService shipBox = new ShippingService(5);
            Console.WriteLine("Shipping price for box:" + shipBox.GetPrice(listProducts));
            Console.WriteLine("Shipping price for book:" + shipBox.GetPrice(book));

            Console.ReadKey();
        }
    }
}
