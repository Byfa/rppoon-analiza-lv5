﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class ShippingService
    {
        private double pricePerKG;
        public ShippingService(double pricePerKG)
        {
            this.pricePerKG = pricePerKG;
        }
        public double GetPrice(IShipable products)
        {
            return products.Weight * this.pricePerKG;
        }
    }
}
