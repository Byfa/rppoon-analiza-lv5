﻿using LV5_ZAD4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_ZAD3
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }

        public void ConsolePrinter(IDataset data)
        {
            IReadOnlyCollection<List<string>> temporaryData = data.GetData();
            if (temporaryData != null)
            {
                foreach (List<string> list in temporaryData)
                {
                    list.ForEach(i => Console.Write(i + " "));
                    Console.WriteLine(" ");
                }
            }
            else
                Console.WriteLine("ERROR. GetData() returned NULL.");
        }
    }
}
