﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using LV5_ZAD4;

namespace LV5_ZAD3
{
    class Dataset : IDataset
    {
        private string filePath;
        private List<List<string>> data;
        public Dataset(string filePath)
        {
            this.filePath = filePath;
            this.data = new List<List<string>>();
            this.LoadDataFromCSV();
        }
        private void LoadDataFromCSV()
        {
            string[] lines = System.IO.File.ReadAllLines(this.filePath);
            foreach (string line in lines)
            {
                List<string> row = new List<string>(line.Split(','));
                data.Add(row);
            }
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            return new ReadOnlyCollection<List<string>>(this.data);
        }
        public string GetStringRepresentation()
        {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            foreach (List<string> list in this.data)
            {
                foreach(string s in list)
                {
                    stringBuilder.Append(s);
                }
                stringBuilder.Append("\n");
            }
            return stringBuilder.ToString();
        }

    }
}
