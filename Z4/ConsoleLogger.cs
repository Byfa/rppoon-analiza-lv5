﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_ZAD4
{
    class ConsoleLogger
    {
        static ConsoleLogger instance;

        private ConsoleLogger()
        {
        }
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
                instance = new ConsoleLogger();
            return instance;
        }
        public void Log()
        {
            Console.WriteLine("GetData() has been called: " + DateTime.Now);
        }
    }
}
