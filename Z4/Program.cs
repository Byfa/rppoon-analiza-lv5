﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LV5_ZAD4;

namespace LV5_ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            //User user1, user2;
            //user1 = User.GenerateUser("User one");
            //user2 = User.GenerateUser("User two");

            Dataset dataset = new Dataset("datoteka.csv");
            //Console.WriteLine("Output using object from class Dataset:");
            //DataConsolePrinter dataConsolePrinter = new DataConsolePrinter(dataset);

            //Console.WriteLine("\nOutput using object from class Dataset using virtual Proxy:");
            //VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("datoteka.csv");
            //dataConsolePrinter = new DataConsolePrinter(virtualProxyDataset);

            //Console.WriteLine("\nOutput using object from class Dataset using protection Proxy, user ID=1:");
            //ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user1);
            //dataConsolePrinter = new DataConsolePrinter(protectionProxyDataset);

            //Console.WriteLine("\nOutput using object from class Dataset using protection Proxy, user ID=2:");
            //protectionProxyDataset = new ProtectionProxyDataset(user2);
            //dataConsolePrinter = new DataConsolePrinter(protectionProxyDataset);
            ////pošto je dva, ne moze se ispisati jer GetData() daje null


            ////u prijasnjim primjerima ConsoleLogger se koristi bez proxy, a u sljedecim s njime
            //VirtualProxyLoggingDataset virtualProxyLoggingDataset = new VirtualProxyLoggingDataset("datoteka.csv");
            //dataConsolePrinter = new DataConsolePrinter(virtualProxyLoggingDataset);

            IDataset virtualProxyLoggingDataset = new VirtualProxyLoggingDataset("datoteka.csv");
            IReadOnlyCollection<List<string>> data = virtualProxyLoggingDataset.GetData();

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();
            dataConsolePrinter.ConsolePrinter(virtualProxyLoggingDataset);

            Console.ReadKey();
        }
    }
}
