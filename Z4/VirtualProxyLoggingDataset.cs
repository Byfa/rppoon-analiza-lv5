﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LV5_ZAD4;
using LV5_ZAD3;
using System.Collections.ObjectModel;

namespace LV5_ZAD4
{
    class VirtualProxyLoggingDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;

        public VirtualProxyLoggingDataset(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            ConsoleLogger.GetInstance().Log();
            return dataset.GetData();
        }
    }
}
